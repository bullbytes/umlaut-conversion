open Str

(* Maps placeholders to their replacements. *)
let placeholders_and_replacements = [("ue", "ü"); ("oe","ö"); ("ae","ä"); ("Ue", "Ü"); ("Oe","Ö"); ("Ae","Ä"); ("sz","ß")] 

(* We won't replace the placeholders in these words, case insensitive. *)
let default_ignored_words = ["lasziv"; "faszin.*"; ".+eue.*"; "goethe"; "aktuell.*"; ".*aue.*"; "zuerst"]

(* Optionally, returns the first occurrence of regex in str, starting at start. Returns an empty Option if the regex is not found. *)
let search_forward_opt regex str start =
  try Some(search_forward regex str start)
  with Not_found -> None

(* Returns true if the str contains another string defined by the regex. *)
let contains regex str =
  match search_forward_opt regex str 0 with
  Some(_) -> true
  | None -> false

(* Returns true if the words is among the given ignored_words *)
let is_ignored word ignored_words =
  (* The ignored words are regexes of words that contain placeholders like "ue" or "sz" that we won't convert. We ignore case of the word to check using regexp_case_fold. *)
  List.exists (fun ignore_word_regex ->
    string_match (regexp_case_fold ignore_word_regex) word 0) ignored_words

(* Returns true if the word contains a placeholder like "ue" or "sz". *)
let contains_placeholder word = 
  let placeholders = List.map (fun (fst,snd) -> fst) placeholders_and_replacements in
  List.exists (fun placeholder -> contains (regexp placeholder) word) placeholders

(* Replaces all placeholders in the word. For example if the word is "schoen" returns "schön". *)
let replace_placeholders word =
  List.fold_left (fun word_to_change (placeholder, replacement) ->
    global_replace (regexp_string placeholder) replacement word_to_change
  ) word placeholders_and_replacements

(* Returns a map from each word with a placeholder to the replaced word. For example if the words consist of ["schoen"], this function returns [("schoen", "schön")] *)
let get_replacements words_to_replace ignored_words =
  List.fold_left (fun replacements word ->
    if contains_placeholder word && not (is_ignored word ignored_words) then 
      (word, replace_placeholders word) :: replacements
    else
      replacements
    ) [] words_to_replace

(* Replaces the placeholders ("ue", "Ae" or "sz") in each word of the line. *)
let rec convert_line line ignored_words =
  (* \b matches word boundaries *)
  let words_to_replace = split (regexp "\\b") line in
  (* Replace the placeholders in each word *)
  let original_words_and_changed_words = get_replacements words_to_replace ignored_words in
  (* Replace the original words with their replacement in the line *)
  List.fold_left (fun line_to_change (word, changed_word) ->
    (* regexp_string matches the exact string given, in case the changed word contains regex characters like + or \. *)
      global_replace (regexp_string word) changed_word line_to_change
  ) line original_words_and_changed_words

(* Reads the lines of the file at the given path. *)
let read_lines path =
  let rec read_lines channel = 
    try
      let line = input_line channel in
      line :: (read_lines channel)
    with
    End_of_file -> []
  in
  let channel = open_in path in
  read_lines channel

(* Replaces each word with placeholders ("ue", "Ae" or "sz") in the text file at the given path. *)
let convert_content path ignored_words = 
  let lines = read_lines path in
  List.map(fun line -> convert_line line ignored_words) lines

let print_converted_lines file_to_convert ignored_words =
  List.iter (fun line -> Printf.printf "%s\n" line) (convert_content file_to_convert ignored_words)

let words_to_ignore_flag = "--words-to-ignore"
let print_default_words_to_ignore_flag = "--print-default-words-to-ignore"

let print_usage () = let usage_text = "Usage:\n\tumlaut_conversion <file-with-text-to-convert> ["^words_to_ignore_flag^"=<file>]\n\tumlaut_conversion "^ print_default_words_to_ignore_flag ^"\nOptions:\n\t"^words_to_ignore_flag^"=<file>\tA file containing a regular expression on each line. Words matching these expressions, ignoring case, will not be converted even if the word contains placeholders like 'ae' or 'sz'.\n\t" ^ print_default_words_to_ignore_flag ^"\tPrint the default words that are not converted.\n" in
print_string usage_text

let print_description () = print_string "Converts placeholders in text into German umlauts and into sharp s.\nPlaceholders are ae, oe, ue, and sz which are converted to ä, ö, ü, and ß.\nFor example: 'Schoene Gruesze' -> 'Schöne Grüße'\n\n"

let print_default_words_to_ignore () = List.iter (fun ignored_word -> Printf.printf "%s\n" ignored_word) default_ignored_words

let split_at_first delimiter str =
  let open String in
  match search_forward_opt (regexp_string delimiter) str 0 with
  Some(index_of_delim) ->
    let first_half = sub str 0 index_of_delim in
    let start_of_second_half = index_of_delim + 1 in
    let second_half = sub str start_of_second_half (length str - start_of_second_half) in
    Some (first_half, second_half)
  | None -> None

let () =
  match Sys.argv with
  [|_; first_arg |] ->
    (match first_arg with
    flag when flag = print_default_words_to_ignore_flag -> print_default_words_to_ignore ()
    | "--help" | "-h" -> print_description (); print_usage ()
    | file_to_convert -> print_converted_lines file_to_convert default_ignored_words
    )
  | [|_;file_to_convert; second_arg|] ->
     (match (split_at_first "=" second_arg) with
     Some(flag, file_with_words_to_ignore) when flag = words_to_ignore_flag -> print_converted_lines file_to_convert (read_lines file_with_words_to_ignore)
     | _ -> print_usage ()
     )
  | _ -> print_usage ()

