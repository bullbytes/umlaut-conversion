A command-line tool that converts placeholders in text into German umlauts and into sharp s. Placeholders are ae, oe, ue, and sz which are converted to ä, ö, ü, and ß.

For example: "Schoene Gruesze" → "Schöne Grüße"

# Compile and Run
You'll need OCaml's package manager [OPAM](https://opam.ocaml.org/) and a suitable OCaml compiler to compile and run this program.

On Arch Linux, for example, to install OPAM and get its dependency resolver [clingo](https://potassco.org/clingo), enter

    pacman -Sy opam clingo

Then, initialize OPAM and install required build tools:

    opam init; opam install ocamlbuild ocamlfind

To compile this program, get and initialize an OCaml compiler:

    opam switch 4.05.0; eval $(opam config env)

Then compile and run this program:
  
    ocamlbuild -use-ocamlfind umlaut_conversion.byte
    ./umlaut_conversion.byte /home/me/text_to_convert.txt

If you want to convert text from another source than a file, you can use [process substitution](https://www.gnu.org/software/bash/manual/bashref.html#Process-Substitution):

    ./umlaut_conversion.byte <(echo "Schoene Gruesze von Goethe!")

which produces

> Schöne Grüße von Goethe!

# Vim Integration
<img src="readme_files/demo.gif" alt="Showcase of converting text with placeholders into text with umlauts and sharp s" width="60%">

Add this function to your `.vimrc` to convert selected lines (or the current line if none is selected):

    " -range allows to give a range of lines to the command, default is the
    " current line. See h:command-range
    command! -range Umlauts <line1>,<line2>call ConvertPlaceholdersToGermanCharacters()
    function! ConvertPlaceholdersToGermanCharacters() range
      let scriptPath = $HOME . "/path/to/umlaut_conversion/umlaut_conversion.byte"

      " Join the lines from the range using the new line character
      let textToConvert = join(getline(a:firstline, a:lastline), "\n")

      " The external command expects a file path as input; we use process
      " substitution to pass it our text and to avoid creating a temporary file manually
      let convertedText = system(scriptPath . " " . "<(echo '" . textToConvert . "')")

      " Move the cursor to the line where the selection ends
      :execute a:lastline

      :put = convertedText
    endfunction

Adapt the `scriptPath` accordingly.
